defmodule PigLatin do
  @doc """
  Given a `phrase`, translate it a word at a time to Pig Latin.

  Words beginning with consonants should have the consonant moved to the end of
  the word, followed by "ay".

  Words beginning with vowels (aeiou) should have "ay" added to the end of the
  word.

  Some groups of letters are treated like consonants, including "ch", "qu",
  "squ", "th", "thr", and "sch".

  Some groups are treated like vowels, including "yt" and "xr".
  """
  @spec translate(phrase :: String.t()) :: String.t()
  def translate(phrase) do
    alphabet = for n <- ?a..?z, do: <<n::utf8>>
    vowels = ["a", "e", "i", "o", "u"]
    qu = ["qu"]
    vowels_qu = ["aqu", "equ", "iqu", "oqu", "uqu"]
    consonant = alphabet -- vowels
    consonant_and_qu = Enum.map(consonant, fn conso -> "#{conso}qu" end)

    x_y_followed_consonant =
      Enum.map(consonant, fn conso -> ["x#{conso}", "y#{conso}"] end) |> List.flatten()

    Enum.map(String.split(phrase, " ", trim: true), fn phrase ->
      cond do
        String.starts_with?(phrase, vowels) ->
          "#{phrase}ay"

        String.starts_with?(phrase, vowels_qu) ->
          "#{phrase}ay"

        String.starts_with?(phrase, qu) ->
          lista = String.split(phrase, ~r{qu}, include_captures: true, trim: true)
          tail_of_the_list = tl(lista)
          head_of_the_list = hd(lista)
          "#{tail_of_the_list}#{head_of_the_list}ay"

        String.starts_with?(phrase, consonant_and_qu) ->
          hrase = String.split(phrase, consonant_and_qu, trim: true)
          body = String.slice(phrase, 0..2)
          "#{hrase}#{body}ay"

        String.starts_with?(phrase, x_y_followed_consonant) ->
          "#{phrase}ay"

        String.starts_with?(phrase, consonant) ->
          length = String.length(phrase)
          phrase_code_points = String.codepoints(phrase)

          starting =
            Enum.reduce_while(phrase_code_points, 0, fn x, acc ->
              if String.contains?(x, consonant), do: {:cont, acc + 1}, else: {:halt, acc}
            end)

          hrase = String.slice(phrase, starting..length)
          initial_hrase = String.slice(phrase, 0, starting)
          "#{hrase}#{initial_hrase}ay"
      end
    end)
    |> Enum.join(" ")
  end
end
